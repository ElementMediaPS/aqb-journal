<?php
$Page_Title = "AQB Journal Article";
$Custom_Styles = "";
include "inc/header.php";
if($Article_ID != NULL) {
    $Article_ID = $_GET['id'];
} else {
    echo "You must select id"; 
    exit;
}
?>
<style>
    .box {
        opacity: 1 !important;
        background-image: linear-gradient(to bottom, rgba(92, 19, 153, 0.5), rgba(51, 36, 116, 0.5));
    }
</style>

<div class="d-none mt-5 text-center">
    <a href="single-print.php?id=<?php echo $Article_ID; ?>" class="text-white" style="font-size: 30px;text-decoration:none;">
        <i class="fas fa-print"></i>
        <p class="text-white" style="font-size:12px; font-weight: 500;">نسخة للطباعة</p>
    </a>
</div>
<div class="col-md-11 col-10 mt-5">
    <h1 class="text-white text-ar text-right text-shadow" style="font-weight:700;"><?php echo $Article[$Article_ID]['title']; ?></h1>
    <h4 class="text-white text-ar text-right text-shadow">بقلم: رلى أبو دحو</h4>
</div>
<div class="col-md-1 col-2 mt-5">
    <a id="link" href="index.php" class="text-white" style="font-size: 60px;"><i class="fas fa-long-arrow-alt-right"></i></a>
</div>
<div class="col-12 pt-5 text-white text-ar text-justify">
    <img src="uploads/<?php echo $Article[$Article_ID]['thumb']; ?>" alt="" class="Article_Featured_Image" />
    <?php echo $Article[$Article_ID]['description']; ?>
</div>
<div class="sharing_section col-12 mt-5 pt-3 text-center text-ar">
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f44dd7bf4be8368"></script>
    <div class="Share_Text">
        <a type="button" class="btn btn-primary mb-2"><i class="fas fa-share-alt"></i> Share</a>
    </div>
    <div class="Share_Buttons">
        <div class="addthis_inline_share_toolbox"></div>
    </div>
</div>

<?php include "inc/footer.php"; ?>
