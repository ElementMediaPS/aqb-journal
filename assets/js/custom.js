$(window).scroll(function () {
    var s = $(window).scrollTop(),
        opacityVal = (s / 150.0);
    $('.box').css('opacity', opacityVal);
});
/*$(function() {
    $('#ScrollDown').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 500, 'linear');
    });
});*/

$(document).ready(function () {
    $(".linkscroll").click(function (event) {
        event.preventDefault();
        linkLocation = this.href;
        $('.card').addClass('fadeOutDown').delay(1000).queue(function () {
            window.location = linkLocation;
        });
    });
});

function scrollToHash(hashName) {
    location.hash = "#" + hashName;
    $('html, body').animate({
        scrollTop: $(location.hash).offset().top
    }, 500, 'linear');
}
var elem = document.querySelector('.grid');
var iso = new Isotope(elem, {
    // options
    itemSelector: '.grid-item',
    layoutMode: 'fitRows'
});

// element argument can be a selector string
// init Isotope
var $grid = $('.grid').isotope({
    // options
});

// layout Isotope after each image loads
$grid.imagesLoaded().progress( function() {
  $grid.isotope('layout');
});

// filter items on button click
$('#Categories_Menu').on('click', 'button', function () {
    var filterValue = $(this).attr('data-filter');
    $grid.isotope({
        filter: filterValue
    });
});


function openSearch() {
    $("#SearchWindow").fadeIn('slow');
}

function closeSearch() {
    $("#SearchWindow").fadeOut('slow');
}


jQuery(document).ready(function ($) {
    var bsDefaults = {
            offset: false,
            overlay: true,
            width: '330px'
        },
        bsMain = $('.bs-offset-main'),
        bsOverlay = $('.bs-canvas-overlay');

    $('[data-toggle="canvas"][aria-expanded="false"]').on('click', function () {
        var canvas = $(this).data('target'),
            opts = $.extend({}, bsDefaults, $(canvas).data()),
            prop = $(canvas).hasClass('bs-canvas-right') ? 'margin-right' : 'margin-left';

        if (opts.width === '100%')
            opts.offset = false;

        $(canvas).css('width', opts.width);
        if (opts.offset && bsMain.length)
            bsMain.css(prop, opts.width);

        $(canvas + ' .bs-canvas-close').attr('aria-expanded', "true");
        $('[data-toggle="canvas"][data-target="' + canvas + '"]').attr('aria-expanded', "true");
        if (opts.overlay && bsOverlay.length)
            bsOverlay.addClass('show');
        return false;
    });

    $('.bs-canvas-close, .bs-canvas-overlay').on('click', function () {
        var canvas, aria;
        if ($(this).hasClass('bs-canvas-close')) {
            canvas = $(this).closest('.bs-canvas');
            aria = $(this).add($('[data-toggle="canvas"][data-target="#' + canvas.attr('id') + '"]'));
            if (bsMain.length)
                bsMain.css(($(canvas).hasClass('bs-canvas-right') ? 'margin-right' : 'margin-left'), '');
        } else {
            canvas = $('.bs-canvas');
            aria = $('.bs-canvas-close, [data-toggle="canvas"]');
            if (bsMain.length)
                bsMain.css({
                    'margin-left': '',
                    'margin-right': ''
                });
        }
        canvas.css('width', '');
        aria.attr('aria-expanded', "false");
        if (bsOverlay.length)
            bsOverlay.removeClass('show');
        return false;
    });
});