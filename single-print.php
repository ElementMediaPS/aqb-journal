<?php
$Page_Title = "AQB Journal Article";
$Custom_Styles = "";
include "data.php";
if(isset($_GET['id']) AND is_numeric($_GET['id']) ) {
    $Article_ID = $_GET['id'];
} else {
    echo "You must select id"; 
    exit;
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Al Quds Bard University Journal Website">
    <meta name="author" content="Element Media">
    <meta name="generator" content="Element Media">
    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title><?php echo $Page_Title; ?></title>
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/fonts.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
    <script>
        function fadeInPage() {
            if (!window.AnimationEvent) {
                return;
            }
            var fader = document.getElementById('fader');
            fader.classList.add('fade-out');
        }

        document.addEventListener('DOMContentLoaded', function() {
            if (!window.AnimationEvent) {
                return
            }

            var anchors = document.getElementsByTagName('a');

            for (var idx = 0; idx < anchors.length; idx += 1) {
                if (anchors[idx].hostname !== window.location.hostname) {
                    continue;
                }

                anchors[idx].addEventListener('click', function(event) {
                    var fader = document.getElementById('fader'),
                        anchor = event.currentTarget;

                    var listener = function() {
                        window.location = anchor.href;
                        fader.removeEventListener('animationend', listener);
                    };
                    fader.addEventListener('animationend', listener);

                    event.preventDefault();
                    fader.classList.add('fade-in');
                });
            }
        });

        window.addEventListener('pageshow', function(event) {
            if (!event.persisted) {
                return;
            }
            var fader = document.getElementById('fader');
            fader.classList.remove('fade-in');
        });

    </script>
</head>

<body>
    <!-- To be used as a color effect when opening the pages -->
    <svg id="fader" style="background-image: url('assets/img/bg1.jpg');"></svg>
    <script>
        fadeInPage()

    </script>
    <div id="homepage" class="container">
        <div class="row">

            <div class="col-md-1 col-2 mt-5">
                <a id="link" href="single.php?id=<?php echo $Article_ID; ?>" class="text-dark" style="font-size: 60px;"><i class="fas fa-long-arrow-alt-left"></i></a>
            </div>
            <div class="col-md-11 col-10 mt-5">
                <h1 class="text-ar text-right" style="font-weight:700;"><?php echo $Article[$Article_ID]['title']; ?></h1>
                <h4 class="text-ar text-right">بقلم: رلى أبو دحو</h4>
            </div>
            <div class="col-12 pt-5 pb-5 text-ar text-justify">
                <img src="uploads/<?php echo $Article[$Article_ID]['thumb']; ?>" alt="" class="Article_Featured_Image" />
                <?php echo $Article[$Article_ID]['description']; ?>
            </div>
            <div class="col-12 col-md-6 text-md-left text-center pt-md-4 pt-4 pb-3">
                <i class="fab fa-facebook-square mr-3"></i>
                <i class="fab fa-linkedin mr-3"></i>
                <i class="fab fa-vimeo"></i>
            </div>
            <div class="col-12 col-md-6 text-md-right text-center text-ar pt-md-4 pt-0 pb-3">
                <small>جميع الحقوق محفوظة لـ منصة حنّون © 2020</small>
            </div>
        </div>
    </div>
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/custom.js"></script>
</body>

</html>
