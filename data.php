<?php 
$Article = array();

$Article[] = array(
    'title' => 'الأسيرات الفلسطينيات في الأسر',
    'category' => 'Orange',
    'thumb' => 'engin-akyurt-l1clu1ZKjSw-unsplash.jpg',
    'type' => 'sound',
    'link' => 'JavaScript:void(0)',
    'description' => ''
);
$Article[] = array(
    'title' => 'عن تجربة السجن والكتابة',
    'category' => 'Pink',
    'thumb' => 'pexels-miguel-á-padriñán-1010973.jpg',
    'type' => 'article',
    'link' => 'JavaScript:void(0)',
    'description' => ''
);
$Article[] = array(
    'title' => 'مساحة المهمشين',
    'category' => 'Yellow',
    'thumb' => 'pexels-jimmy-chan-1309902.jpg',
    'type' => 'video',
    'link' => 'JavaScript:void(0)',
    'description' => ''
);
$Article[] = array(
    'title' => 'عن تجربة الاسر في "عالم ليس لنا"',
    'category' => 'Yellow',
    'thumb' => 'pexels-cameron-casey-1687067.jpg',
    'type' => 'gallery',
    'link' => 'single.php',
    'description' => '<p>بالنسبة إلي، هذا العام لم يكن كغيره من الأعوام التي قاربت عشرين عاما التي امضيتها في جامعة بيرزيت محاضرةً وباحثة. لقد كان عاما قاسيا اجهز خلاله فيروس كوفيد -19 (كورونا) على استمرار السنة الأكاديمية واقتلعها من جذورها، ملقياً بنا بلا رحمة او هوادة في الحجر المنزلي بعيدا عن كل ما له علاقة بالحياة اليومية وصخبها. والجامعة وساحاتها واروقتها التي ضمت بينها الالاف من الطلبة الذين صنعوا لبير زيت على مر العقود اسمها ومكانتها بتفوقهم واجتهادهم، والأهم مواقفهم النضالية والوطنية. وعلى الرغم من التراجع في الكثير من المواقع، ظلت الحركة الطلابية في جامعة بيرزيت عصية على الكسر، وتعيش المحاولات المستمرة للصمود والمقاومة.</p>
    <p>ولكن وان انتهت السنة الأكاديمية في بيوتنا خارج عالمنا الجامعي، فقد كانت بدايتها الأسوأ بالنسبة إلي، فقد اقتلع فيروس اشد فتكاً هو الاحتلال العشرات من طلبة الجامعة وزجهم في اقبية التحقيق حيث خضعوا لتحقيق كان الاقسى الذي يتعرض له المناضلون خلال التحقيق منذ سنوات.</p>
    <p>اعتقال الطلبة لم يكن جديداً ولكن حجمه وشراسته القيا بظلالهما على مجتمع الجامعة والطلبة.</p>
    <p>اما انا وقد اختبرت الزنازين والاسر، فقد كان وقعه علي أقسى، فجزء كبير منهم كانوا طلبتي، كانوا هناك معي في قاعات المحاضرات لمساقات مختلفة، ولفصول عده، واليوم غابوا، هم طلبتي، منهم المشاكس والهادئ والنشيط، والمبدع، ومن يحرص على ان يناقش ويجادل في محاولة منه ان يثبت مقولة "تفوق الطالب على استاذه"، وانا بطبعي المشاكس استفز رغبة النقاش والجدل لديهم ليخرج أفضل ما فيهم. ولكن لا شك اليوم هم أساتذة يعيدون صياغة الدرس عبر عظامهم التي تكسرت وجراحهم التي نزفت في زنازين التحقيق.</p>
    <p>ما حصل هذا العام من الحجر والاعتقال للطلبة، طرق ذاكرتي وعقلي بقوة واستعاد تفاصيل الاسر، والتجربة التي عشتها لسنوات طويلة وطرح علي الكثير من الأسئلة، فانا أيضا كنت في عمرهم عند اعتقالي فلم اتجاوز حينها العشرين بعد، وطالبة على مقاعد الدراسة في جامعة بيت لحم. انتزعت من بين رفاقي ورفيقاتي، واهلي لأمضي سنوات الاسر، حيث تجربة أخرى وحجر اجباري والمستقبل قد تجمد عند لحظة واحده بقيت هي الحاضر.</p>
    <p>في السجن تمتلك الذاكرة صور الماضي. وتعيش حاضراً محدوداً بتفاصيل واقعها المحاصر بين أربعة جدران، اما المستقبل فقد توقف وكأن الساعة على الحائط توقفت عقاربها فيما دقاتها مستمرة. وبين الساعة والماضي والحاضر يختزل "عالم ليس لنا"</p>'
);
$Article[] = array(
    'title' => 'عهد التميمي: "أنا مقاتلة الحرية"',
    'category' => 'Pink',
    'thumb' => 'F180725WHFF06.jpg',
    'type' => 'sound',
    'link' => 'JavaScript:void(0)',
    'description' => ''
);
$Article[] = array(
    'title' => 'محمد بكري يطلق الصمام ويتكلم!',
    'category' => 'Pink',
    'thumb' => '20180506050419.jpg',
    'type' => 'article',
    'link' => 'JavaScript:void(0)',
    'description' => ''
);
$Article[] = array(
    'title' => 'مي مصري: 3000 ليلة هي قصة عدالة',
    'category' => 'Pink',
    'thumb' => '3000_nights.jpg',
    'type' => 'video',
    'link' => 'JavaScript:void(0)',
    'description' => ''
);
$Article[] = array(
    'title' => 'المخرجة مي المصري تطلق قصة جديدة',
    'category' => 'Pink',
    'thumb' => '3000-nights-02gs.jpg',
    'type' => 'gallery',
    'link' => 'JavaScript:void(0)',
    'description' => ''
);

?>