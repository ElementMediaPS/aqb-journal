<?php
include "data.php";
if(isset($_GET['id']) AND is_numeric($_GET['id']) ) {
    $Article_ID = $_GET['id'];
} else {
    $Article_ID = NULL;
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Al Quds Bard University Journal Website">
    <meta name="author" content="Element Media">
    <meta name="generator" content="Element Media">
    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title><?php echo $Page_Title; ?></title>
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/fonts.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
    <script>
        function fadeInPage() {
            if (!window.AnimationEvent) {
                return;
            }
            var fader = document.getElementById('fader');
            fader.classList.add('fade-out');
        }

        document.addEventListener('DOMContentLoaded', function() {
            if (!window.AnimationEvent) {
                return
            }

            var anchors = document.getElementsByTagName('a');

            for (var idx = 0; idx < anchors.length; idx += 1) {
                if (anchors[idx].hostname !== window.location.hostname) {
                    continue;
                }

                anchors[idx].addEventListener('click', function(event) {
                    var fader = document.getElementById('fader'),
                        anchor = event.currentTarget;

                    var listener = function() {
                        window.location = anchor.href;
                        fader.removeEventListener('animationend', listener);
                    };
                    fader.addEventListener('animationend', listener);

                    event.preventDefault();
                    fader.classList.add('fade-in');
                });
            }
        });

        window.addEventListener('pageshow', function(event) {
            if (!event.persisted) {
                return;
            }
            var fader = document.getElementById('fader');
            fader.classList.remove('fade-in');
        });

    </script>
</head>

<body class="issue1">
    <!-- To show the Overlay sidebar when clicking on it from the right -->
    <div class="bs-canvas-overlay bs-canvas-anim bg-dark position-fixed w-100 h-100"></div>
    <!-- To be used as a color effect when opening the pages -->
    <svg id="fader" style="background-image: url('assets/img/bg1.jpg');"></svg>
    <script>fadeInPage()</script>
    <!-- To make the background blurry -->
    <div class="box"></div>
    <!-- To show the Accessibility Icon on the right -->
    <div class="Accessibility_Icon">
        <div class="c-share">
            <input class="c-share__input" type="checkbox" id="checkbox">
            <label class="c-share__toggler" for="checkbox">
                <img src="assets/img/accessibility.png" alt="" title="Options to make the website accessibale" />
            </label>
            <ul class="c-share_options" data-title="Accessibility">
                <li>Voice Over</li>
                <li>Reader Version</li>
                <li>Blindcolor Version</li>
                <li>Text: Aa+ | Aa-</li>
            </ul>
        </div>
    </div>
    <?php if($Article_ID != NULL) { ?>
    <div class="Reader_Version">
        <div class="c-share">
            <a href="single-print.php?id=<?php echo $Article_ID; ?>" title="Read the article in a clearer way">
                <img src="assets/img/reader-version.png" alt="" />
            </a>
        </div>
    </div>
    <?php } ?>
    
    <div class="Issue_Name">
        <span class="text-white text-ar mr-4">الأسرى الفلسطينيين</span>
        <span class="text-white text-en">Palestinian Prisoners</span>
    </div>
    <div id="homepage" class="container">
        <div class="row">
            <div id="bs-canvas-right" class="bs-canvas bs-canvas-anim bs-canvas-right position-fixed bg-light h-100" data-width="400px">
                <header class="bs-canvas-header p-3 bg-primary overflow-auto">
                    <button type="button" class="bs-canvas-close float-left close" aria-label="Close"><span aria-hidden="true" class="text-light">&times;</span></button>
                    <h4 class="d-inline-block text-light mb-0 float-right">الإصدار الأول</h4>
                </header>
                <div class="bs-canvas-content px-3 py-5 text-ar text-justify">
                    <h3><b>الأسرى الفلسطينيين</b></h3>
                    <p>يتحدث الإصدار الأول عن الأسرى الفلسطينين في سجون الإحتلال حيث يتكلم بشكل مفصل عن تجارب الطلاب والعائلات التي لديها أسرى</p>
                    <p>يتحدث الإصدار الأول عن الأسرى الفلسطينين في سجون الإحتلال حيث يتكلم بشكل مفصل عن تجارب الطلاب والعائلات التي لديها أسرى</p>
                    <p>يتحدث الإصدار الأول عن الأسرى الفلسطينين في سجون الإحتلال حيث يتكلم بشكل مفصل عن تجارب الطلاب والعائلات التي لديها أسرى</p>
                    <br />
                    <h5><b>إصداراتنا الأخرى</b></h5>
                    <a href="#" class="btn btn-primary btn-block text-right">الإصدار الثاني: فلسطين والتقدم</a>
                    <a href="#" class="btn btn-primary btn-block text-right">الإصدار الثالث: كيف أرى فلسطين بنظري</a>
                    <a href="#" class="btn btn-primary btn-block text-right">الإصدار الرابع: كيف أرى فلسطين</a>
                </div>
            </div>

            <div class="col-12 text-center mt-3">
                <ul class="Main_Menu text-ar">
                    <li><a href="javascript:void(0);" data-toggle="canvas" data-target="#bs-canvas-right" aria-expanded="false" aria-controls="bs-canvas-right">عن الإصدار الأول</a></li>
                    <li><a href="#">عن حنّون</a></li>
                    <li><a href="#">المقالات</a></li>
                    <li><a href="#">إتصل بنا</a></li>
                    <li><a href="javascript:void(0);" onclick="openSearch();"><i class="fas fa-search"></i></a></li>
                </ul>
            </div>
