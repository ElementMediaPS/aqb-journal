<div class="col-12 col-md-4 text-white text-ar text-justify mt-5">
    <h4>عن حنّون</h4>
    <p class="pt-2">حنّون تعتبر مجلة مميزة تتحدث عن الأسرى والسياسة ولدينا مجموعة من الإصدارات التي سيتم مشاهدها وقرأتها يوميا بإدخال الطلبة والمعلمين حنّون تعتبر مجلة مميزة تتحدث عن الأسرى والسياسة ولدينا مجموعة من الإصدارات التي سيتم مشاهدها وقرأتها يوميا بإدخال الطلبة والمعلمين</p>
</div>
<div class="col-12 col-md-4 text-white text-ar text-justify mt-5">
    <h4>أحدث المقالات في حنّون</h4>
    <ul class="footer-articles pt-1">
        <li><a href="#">عن تجربة السجن والكتابة</a></li>
        <li><a href="#">عن تجربة الأسر في "عالم ليس لنا"</a></li>
        <li><a href="#">المخرجة مي المصري تطلق قصة جديدة</a></li>
        <li><a href="#">محمد البكري يطلق الصمام ويتكلم</a></li>
        <li><a href="#">مي المصري: 3000 ليلة هي قصة عدالة</a></li>
    </ul>
</div>
<div class="col-12 col-md-4 text-white text-ar text-justify mt-5">
    <h4>شارك في القائمة البريدية</h4>
    <div class="row no-gutters">
        <div class="col-6 pl-1">
            <input type="text" value="" placeholder="الأسم الأول" class="form-control mt-3">
        </div>
        <div class="col-6 pr-1">
            <input type="text" value="" placeholder="الأسم الأخير" class="form-control mt-3">
        </div>
        <div class="col-12">
            <input type="text" value="" placeholder="البريد الإلكتروني" class="form-control mt-3">
        </div>
        <div class="col-12">
            <button type="submit" value="Add" class="btn btn-primary btn-block mt-3">الإنضمام</button>
        </div>
    </div>
</div>
<div class="col-12 col-md-6 text-md-left text-center text-white pt-md-4 pt-4 pb-3">
    <i class="fab fa-facebook-square mr-3"></i>
    <i class="fab fa-linkedin mr-3"></i>
    <i class="fab fa-vimeo"></i>
</div>
<div class="col-12 col-md-6 text-md-right text-center text-ar text-white pt-md-4 pt-0 pb-3">
    <small>جميع الحقوق محفوظة لـ منصة حنّون © 2020</small>
</div>
</div>
</div>
<div id="SearchWindow" class="overlay">
    <span class="closebtn" onclick="closeSearch()" title="Close Overlay">×</span>
    <div class="overlay-content">
        <form action="/action_page.php">
            <input type="text" placeholder="Search.." name="search">
            <button type="submit"><i class="fa fa-search"></i></button>
            <div class="Search_Issues_Options">Search in:
                <input type="radio" id="current" name="Where_To_Search" value="current"> <label for="current">Current Issue</label>
                <input type="radio" id="all" name="Where_To_Search" value="all"> <label for="all">All Issues</label>
            </div>
        </form>
    </div>
</div>
<script src="assets/js/jquery-3.5.1.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!--<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>-->
<script src="https://unpkg.com/imagesloaded@4.1.4/imagesloaded.pkgd.min.js"></script>
<script src="https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js"></script>
<script src="assets/js/custom.js"></script>

<!-- <script src="https://code.responsivevoice.org/responsivevoice.js?key=JYt4L6Ct"></script> -->

</body>

</html>
