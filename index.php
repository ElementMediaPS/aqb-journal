<?php
$Page_Title = "AQB Journal";
$Custom_Styles = "";
include "inc/header.php";
?>

<div class="col-12 mb-4 text-center">
    <img src="assets/img/hannoun-logo.png" class="img-fluid" style="max-width:180px;" alt="" />
    <h1 class="journal_title_ar" style="font-size: 64px;">حنون</h1>
    <h1 class="journal_title_en mb-4" style="font-size: 18px;">Hannoun</h1>
    <h1 class="journal_title_ar" style="font-size: 70px;font-weight:500;">الأسرى الفلسطينيين</h1>
    <h1 class="journal_title_en" style="font-size: 22px;">Palestinian Prisoners</h1>
    <a id="ScrollDown" class="linkscroll" href="JavaScript:void(0)" onclick="scrollToHash('Letter_From_Editor')"><i class="fas fa-long-arrow-alt-down text-white mt-4" style="font-size: 30px;"></i></a>
</div>

<div id="Letter_From_Editor" class="col-12 mt-5 mb-2 p-3 text-justify text-en text-white">
    <img src="assets/img/announcement.png" alt="Letter of the editor section" />
    <h4>Letter from the Editor</h4>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.<a href="#">Read more.</a></p>
</div>

<div id="Categories_Menu" class="col-12 mt-5 mb-2">
    <button data-filter=".Orange" class="btn btn-sm pt-2 pb-2 Cat-Item text-en Orange">Features</button>
    <button data-filter=".Pink" class="btn btn-sm pt-2 pb-2 Cat-Item text-en Pink">Interviews</button>
    <button data-filter=".Yellow" class="btn btn-sm pt-2 pb-2 Cat-Item text-en Yellow">Profiles</button>
    <button data-filter=".LightBlue" class="btn btn-sm pt-2 pb-2 Cat-Item text-en LightBlue">Screening Room</button>
    <button data-filter=".Red" class="btn btn-sm pt-2 pb-2 Cat-Item text-en Red">Echo Chamber</button>
    <button data-filter=".DarkBlue" class="btn btn-sm pt-2 pb-2 Cat-Item text-en DarkBlue">Reviews</button>
    <button data-filter=".Green" class="btn btn-sm pt-2 pb-2 Cat-Item text-en Green">Emerging Artists</button>
    <button data-filter="*" class="btn btn-sm pt-2 pb-2 Cat-Item text-en White">Show All</button>
</div>

<div class="col-12 p-0">
    <div class="grid">
        <?php for ($x = 0; $x < count($Article); $x++) { ?>
        <div class="grid-item <?php echo $Article[$x]['category']; ?>">
            <div class="masonry-item">
                <a href="single.php?id=<?php echo $x; ?>" class="article-box <?php echo $Article[$x]['category']; ?>">
                    <img src="uploads/<?php echo $Article[$x]['thumb']; ?>">
                    <img class="article-icon" src="assets/img/icon-<?php echo $Article[$x]['type']; ?>.png" alt="" />
                    <div class="article-title text-center text-ar"><?php echo $Article[$x]['title']; ?></div>
                </a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php include "inc/footer.php"; ?>
